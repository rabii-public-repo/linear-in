package com.linear.in;

/*
 * @see https://codingbat.com/prob/p134022
 */
public class LinearIn {
	static boolean linearIn(int[] outer, int[] inner) {
		if (inner.length == 0) {
			return true;
		}
		if (outer[0] > inner[0] || outer[outer.length - 1] < inner[inner.length - 1]) {
			return false;
		}
		for (int i = 0; i < inner.length; i++) {
			int begin = i, end = outer.length;
			int mid = (begin + end) / 2;
			int middle = outer[outer.length / 2];
			while (begin <= mid) {
				if (inner[i] < middle) {
					end = mid - 1;
					mid = (begin + end) / 2;
					middle = outer[mid];
				} else if (inner[i] > middle) {
					begin = mid + 1;
					mid = (begin + end) / 2;
					middle = outer[mid];
				} else {
					break;
				}
			}
			if (inner[i] != middle) {
				return false;
			}
		}
		return true;
	}
}
